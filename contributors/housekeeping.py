import django_housekeeping as hk
from django.conf import settings
from contributors.models import User, Identifier
import logging
import os
import ldap3
import json
import six
from . import models as cmodels

log = logging.getLogger(__name__)

STAGES = ["backup", "harvest", "main", "association", "usermerging", "aggregation", "stats"]

DATA_DIR = getattr(settings, "DATA_DIR", None)
LDAP_DEBIAN_URI = getattr(settings, "LDAP_DEBIAN_URI", None)
LDAP_ALIOTH_URI = getattr(settings, "LDAP_ALIOTH_URI", None)

UID_ALIOTH_DOMAIN = "@users.alioth.debian.org"
UID_DEBIAN_DOMAIN = "@debian.org"


class BackupAssociations(hk.Task):
    """
    Backup user/identifier associations
    """
    def run_backup(self, stage):
        if not self.hk.outdir: return
        outfile = os.path.join(self.hk.outdir.path("backup"), "associations.json")

        res = []
        for ident in cmodels.Identifier.objects.filter(user__isnull=False):
            res.append({
                "type": ident.type,
                "name": ident.name,
                "user": ident.user.email,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class BackupUsers(hk.Task):
    """
    Backup User info
    """
    def run_backup(self, stage):
        if not self.hk.outdir: return
        outfile = os.path.join(self.hk.outdir.path("backup"), "users.json")

        res = []
        for user in cmodels.User.objects.all():
            res.append({
                "email": user.email,
                "full_name": user.full_name,
                "hidden": user.hidden,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class BackupIdentifiers(hk.Task):
    """
    Backup Identifiers info
    """
    def run_backup(self, stage):
        if not self.hk.outdir: return
        outfile = os.path.join(self.hk.outdir.path("backup"), "identifiers.json")

        res = []
        for ident in cmodels.Identifier.objects.all():
            res.append({
                "type": ident.type,
                "name": ident.name,
                "hidden": ident.hidden,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


class UserInfo(object):
    """
    Collect emails and fingerprints of a user in Alioth and Debian LDAP
    databases.

    Members:
         uid: User ID of the user in LDAP, with the domain appended. This can be
              user@users.alioth.debian.org, user-guest@users.alioth.debian.org
              or user@debian.org
      emails: List of emails found for this user
        fprs: List of GPG key fingerprints found for this user
          fn: Full name of the user
    """
    def __init__(self, uid=None, emails=[], fprs=[], fn=""):
        self.uid = uid
        if not emails:
            self.emails = { uid }
        else:
            self.emails = set(emails)
        self.fprs = set(fprs)
        self.fn = fn

    def __repr__(self):
        return "UserInfo('{}', emails={{{}}}, fprs={{{}}}, fn='{}')".format(
            self.uid, ", ".join(self.emails), ", ".join(self.fprs), self.fn)

    def _clean(self, s):
        """
        Convert a string to unicode if needed, and strip all spaces.
        """
        if isinstance(s, six.binary_type):
            s = s.decode("utf-8")
        return s.strip(" \t\n")

    def set_full_name(self, s):
        """
        Set the full name, converting to unicode and stripping spaces if
        needed.

        If s is empty after normalisation, it does nothing.
        """
        s = self._clean(s)
        if not s: return
        self.fn = s

    def add_email(self, email):
        """
        Add an email address, converting to unicode and stripping spaces if
        needed. An empty email address will be ignored.
        """
        email = self._clean(email)
        if not email: return
        self.emails.add(email)

    def add_fpr(self, fpr):
        """
        Add a fingerprint, converting to unicode and stripping spaces if
        needed. An empty fingerprint will be ignored.
        """
        fpr = self._clean(fpr)
        if not fpr: return
        self.fprs.add(fpr)

    def merge(self, uinfo):
        """
        Merge the information from uinfo into this UserInfo.
        """
        if self.uid != uinfo.uid:
            raise ValueError("Cannot merge two UserInfo objects with different uids")
        self.emails |= uinfo.emails
        self.fprs |= uinfo.emails
        self.fn = self.fn or uinfo.fn


class LDAPUsers(object):
    """
    Information collected about users, indexed by user ID.

    Members:
        uid: dict of all known UserInfo objects indexed by their uid
    """
    def __init__(self):
        self.by_uid = {}

    def add(self, dn, attrs, email_field):
        """
        Add an entry from LDAP.

        dn: ldap DN
        attrs: ldap attributes
        domain: "@debian.org" or "@users.alioth.debian.org"
        email_field: key for the the email field in the attrs dict
        """
        if "uid" not in attrs:
            #print("Skipping entry with no uid:", dn)
            return None

        # Decode the uid, normalising accounts without -guest on alioth to
        # @debian.org uids
        uid = attrs["uid"][0].strip()
        if uid.endswith("-guest"):
            uid += UID_ALIOTH_DOMAIN
        else:
            uid += UID_DEBIAN_DOMAIN

        # Reuse an existing UserInfo or create a new one
        uinfo = self.by_uid.get(uid, None)
        if uinfo is None:
            self.by_uid[uid] = uinfo = UserInfo(uid=uid)

        # Add all email addresses found
        if email_field in attrs:
            emails = attrs[email_field]
            for email in emails:
                uinfo.add_email(email)

        # Add all fingerprints found
        if "keyFingerPrint" in attrs:
            fprs = attrs["keyFingerPrint"]
            for fpr in fprs:
                uinfo.add_fpr(fpr)

        return uinfo

    def add_debian(self, dn, attrs):
        """
        Add user data from debian.org's LDAP
        """
        uinfo = self.add(dn, attrs, "emailForward")
        if uinfo is None: return
        uinfo.set_full_name(attrs["gecos"][0].split(b",")[0])

    def add_alioth(self, dn, attrs):
        """
        Add user data from alioth's LDAP
        """
        uinfo = self.add(dn, attrs, "debGforgeForwardEmail")
        if uinfo is None: return
        uinfo.set_full_name(attrs["cn"][0])

    def add_uinfo(self, uinfo):
        """
        Add user data from a UserInfo entry, merging entries if one already
        exists.
        """
        existing = self.by_uid.get(uinfo.uid, None)
        if existing is None:
            self.by_uid[uinfo.uid] = uinfo
        else:
            existing.merge(uinfo)

    def add_identifier(self, ident):
        """
        Add information from an identifier.

        This is used to mock a user database in a development system, when LDAP
        is not available. In that case, we just consider all identifiers in the
        database as valid and infer a user database from them.
        """
        if ident.type == "email":
            if ident.name.endswith(UID_DEBIAN_DOMAIN):
                self.add_uinfo(UserInfo(uid=ident.name))
            elif ident.name.endswith(UID_ALIOTH_DOMAIN):
                uid = ident.name[:-len(UID_ALIOTH_DOMAIN)]
                if uid.endswith("-guest"):
                    uid = uid + UID_ALIOTH_DOMAIN
                else:
                    uid = uid + UID_DEBIAN_DOMAIN

                # Use ident.name instead of uid for email, so that if we
                # have just converted an alioth uid into a debian one, we still
                # harvest the original email
                self.add_uinfo(UserInfo(uid=uid, emails=[ident.name]))
        elif ident.type == "login":
            if ident.name.endswith("-guest"):
                uid = ident.name + UID_ALIOTH_DOMAIN
            else:
                uid = ident.name + UID_DEBIAN_DOMAIN
            self.add_uinfo(UserInfo(uid=uid))


class UserAccountInfo(hk.Task):
    """
    Collect information about accounts from the Alioth and Debian LDAP databases
    """
    NAME = "user_account_info"

    def __init__(self, *args, **kw):
        super(UserAccountInfo, self).__init__(*args, **kw)

        # Container for mock UserInfo objects that can be added by unit tests
        # before calling Housekeeping.run()
        self.mock_data = []

    def run_harvest(self, stage):
        userinfo = LDAPUsers()

        # If we have mock data set from unit tests, add it
        for uinfo in self.mock_data:
            userinfo.add_uinfo(uinfo)

        if getattr(settings, "DEVEL_SYSTEM", False):
            # If we are in a development environment disconnected from LDAP,
            # use the identifiers found in the database to simulate LDAP
            # entries.
            for i in Identifier.objects.all():
                userinfo.add_identifier(i)
        else:
            # This workaround was needed in wheezy, but it does not seem to be
            # needed in Jessie anymore. Keeping it here for reference in case
            # LDAP breaks again when trying to connect over SSL
            #ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, "/etc/ssl/certs/ca-certificates.crt")

            # Get Debian usernames
            if LDAP_DEBIAN_URI is not None:
                conn = ldap3.Connection(LDAP_DEBIAN_URI, auto_bind=True)
                conn.search(
                        "dc=debian,dc=org",
                        '(&(objectclass=inetOrgPerson)(gidNumber=800)(keyFingerPrint=*))',
                        ldap3.SUBTREE,
                        attributes=["uid", "gecos", "emailForward", "keyFingerPrint"])
                for entry in conn.entries:
                    userinfo.add_debian(entry.entry_get_dn(), entry)

            # Get Alioth usernames
            if LDAP_ALIOTH_URI is not None:
                conn = ldap3.Connection(LDAP_ALIOTH_URI, auto_bind=True)
                conn.search(
                        "ou=Users,dc=alioth,dc=debian,dc=org",
                        "(objectClass=*)",
                        ldap3.SUBTREE,
                        attributes=["uid", "cn", "debGforgeForwardEmail"])
                for entry in conn.entries:
                    userinfo.add_alioth(entry.entry_get_dn(), entry)

            log.info("%d users loaded from Debian and Alioth's LDAP databases", len(userinfo.by_uid))

        self.userinfo = userinfo


class AssociateIdentities(hk.Task):
    """
    Machinery to associate identifiers to uids
    """
    NAME = "associate_identity"

    def __init__(self, *args, **kw):
        super(AssociateIdentities, self).__init__(*args, **kw)
        # Initialise counters for statistics
        self.assoc_users_reused = 0
        self.assoc_users_created = 0
        self.assoc_associated = 0

    def run_stats(self, stage):
        log.info("%s: users_reused: %d", self.IDENTIFIER, self.assoc_users_reused)
        log.info("%s: users_created: %d", self.IDENTIFIER, self.assoc_users_created)
        log.info("%s: users_associated: %d", self.IDENTIFIER, self.assoc_associated)

    def __call__(self, identity, uid, reason=None):
        """
        Called by maintenance tasks as self.hk.associate_identity.

        Associate the given Identity to the user with email=uid.

        A reason can be provided to make it show on the user audit log.
        """
        dry_run = self.hk.dry_run

        # Infer an email address from the uid, if it is not already one
        if "@" in uid:
            email = uid
        elif uid.endswith("-guest"):
            email = uid + UID_ALIOTH_DOMAIN
        else:
            email = uid + UID_DEBIAN_DOMAIN

        # Find the user for uid
        try:
            user = User.objects.get(email=email)
            self.assoc_users_reused += 1
        except User.DoesNotExist:
            # This user does not exist: create it
            if not dry_run:
                user = User.objects.create_user(email)
            else:
                user = None
            self.assoc_users_created += 1

        # Skip the association if it is already associated
        if not user or identity.user and identity.user.pk == user.pk:
            return

        # Do the association
        identity.user = user
        self.assoc_associated += 1

        if not dry_run:
            identity.save()
            if reason is None:
                identity.add_log("auto-associated to {}".format(uid))
            else:
                identity.add_log("auto-associated to {} {}".format(uid, reason))


class AssociateLDAPLogins(hk.Task):
    """
    Associate login identifiers based on LDAP information.
    """
    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def run_association(self, stage):
        userinfo = self.hk.user_account_info.userinfo

        # Associate Users to Login identifiers, validating the login
        # identifiers using the LDAP user IDs
        for i in cmodels.Identifier.objects.filter(type="login", user__isnull=True):
            # Get a full email UID from this login
            if i.name.endswith("-guest"):
                uid = i.name + UID_ALIOTH_DOMAIN
            else:
                uid = i.name + UID_DEBIAN_DOMAIN

            # Ignore identifiers whose login name does not match the UIDs we
            # found in LDAP
            if uid not in userinfo.by_uid: continue

            # Associate the identifier
            self.hk.associate_identity(i, uid)


class AssociateLDAPEmails(hk.Task):
    """
    Associate email identifiers based on LDAP information.
    """
    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def _build_email_to_uid_map(self, userinfo):
        """
        Build the association table from emails to uids.
        """
        # Create a dict mapping emails to uids based on userinfo
        by_email = {}
        # Mails that we decided to not associate
        skip = set()
        for uid, info in userinfo.by_uid.items():
            for email in info.emails:
                existing_uid = by_email.get(email, None)
                if existing_uid is None:
                    by_email[email] = uid
                elif existing_uid.endswith(UID_ALIOTH_DOMAIN) and uid.endswith(UID_DEBIAN_DOMAIN):
                    # We assume that a person recently became DDs and that they
                    # would like contributions to rather be associated to their
                    # @debian.org identity.
                    by_email[email] = uid
                elif existing_uid.endswith(UID_DEBIAN_DOMAIN) and uid.endswith(UID_ALIOTH_DOMAIN):
                    # We assume that a person recently became DDs and that they
                    # would like contributions to keep being associated to
                    # their @debian.org identity.
                    pass
                elif existing_uid.endswith(UID_ALIOTH_DOMAIN) and uid.endswith(UID_ALIOTH_DOMAIN):
                    # The person has two alioth accounts and we cannot choose
                    # which one. Remember to skip this email address when
                    # associating later.
                    skip.add(email)
                elif existing_uid.endswith(UID_DEBIAN_DOMAIN) and uid.endswith(UID_DEBIAN_DOMAIN):
                    log.warning("debian accounts {} and {} forward to the same email {}".format(
                        existing_uid, uid, email))

        # Remove from by_email all addresses that should not be associated
        for email in skip:
            del by_email[email]

        return by_email

    def run_association(self, stage):
        by_email = self._build_email_to_uid_map(self.hk.user_account_info.userinfo)

        # Associate existing unassociated email identifiers to the uids found
        # in by_email
        for i in cmodels.Identifier.objects.filter(type="email", user__isnull=True):
            uid = by_email.get(i.name, None)
            if uid is None: continue
            self.hk.associate_identity(i, uid)


class AssociateLDAPFingerprints(hk.Task):
    """
    Associate email identifiers based on LDAP information.
    """
    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def _build_fpr_to_uid_map(self, userinfo):
        """
        Build the association table from emails to uids.
        """
        # Create a dict mapping fingerprints to uids based on userinfo
        by_fpr = {}
        for uid, info in userinfo.by_uid.items():
            for fpr in info.fprs:
                existing_uid = by_fpr.get(fpr, None)
                if existing_uid is None:
                    by_fpr[fpr] = uid
                else:
                    log.warning("debian accounts {} and {} have the same fingerprint {}".format(
                        existing_uid, uid, fpr))

        return by_fpr

    def run_association(self, stage):
        by_fpr = self._build_fpr_to_uid_map(self.hk.user_account_info.userinfo)

        # Associate existing unassociated fpr identifiers to the uids found
        # in by_fpr
        for ident in cmodels.Identifier.objects.filter(type="fpr", user__isnull=True):
            uid = by_fpr.get(ident.name, None)
            if uid is None: continue
            self.hk.associate_identity(ident, uid, reason="fingerprint found in Debian's LDAP")


class MergeDebianAliothUsers(hk.Task):
    """
    Merge foo@debian and foo@alioth User entries
    """
    DEPENDS = [UserAccountInfo]

    def _build_guest_to_debian_map(self, userinfo):
        """
        Build the association from foo-guest@alioth to bar@debian by looking at
        accounts that forward email to the same place.

        Generates a list of (debian login name, alioth login name) of accounts
        with the same forwarding email.
        """
        # FIXME: this may be a problem if some email provider reuses names for
        # old closed accounts. It would be already a problem, however, with
        # contributions reported for the same email identifier.

        ALIOTH_SUFFIX = "-guest@users.alioth.debian.org"
        # Map alioth -guest account forwarding emails to alioth uids
        alioth_uids_by_email = {}
        # Map debian account forwarding emails to debian uids
        debian_uids_by_email = {}
        for uid, info in userinfo.by_uid.items():
            for email in info.emails:
                if uid.endswith(ALIOTH_SUFFIX):
                    alioth_uids_by_email.setdefault(email, []).append(uid[:-len(UID_ALIOTH_DOMAIN)])
                elif uid.endswith(UID_DEBIAN_DOMAIN):
                    debian_uids_by_email.setdefault(email, []).append(uid[:-len(UID_DEBIAN_DOMAIN)])

        for email in set(alioth_uids_by_email.keys()) & set(debian_uids_by_email.keys()):
            for deblogin in debian_uids_by_email[email]:
                for aliothlogin in alioth_uids_by_email[email]:
                    yield deblogin, aliothlogin

    def run_usermerging(self, stage):
        dry_run = self.hk.dry_run

        # Compute what accounts need to be merged.
        # Build a dict mapping the target Debian account login name to a set of
        # source Alioth account login names.
        merge_into_debian_login = {}

        # Add all non-guest alioth logins to map to the corresponding debian
        # login
        ALIOTH_GUEST_SUFFIX = "-guest@users.alioth.debian.org"
        for u in cmodels.User.objects.all():
            if u.email.endswith(ALIOTH_GUEST_SUFFIX): continue
            if not u.email.endswith(UID_ALIOTH_DOMAIN): continue
            # Work on alioth accounts without -guest
            login = u.email[:-len(UID_ALIOTH_DOMAIN)]
            merge_into_debian_login.setdefault(login, set()).add(login)

        # Add all accounts that forward email to the same place
        for debian_login, alioth_login in self._build_guest_to_debian_map(self.hk.user_account_info.userinfo):
            merge_into_debian_login.setdefault(debian_login, set()).add(alioth_login)

        # Merge all alioth logins into the corresponding debian logins
        for debian_login, alioth_logins in merge_into_debian_login.items():
            try:
                user_debian = cmodels.User.objects.get(email=debian_login + UID_DEBIAN_DOMAIN)
            except cmodels.User.DoesNotExist:
                user_debian = None

            for alioth_login in alioth_logins:
                try:
                    user_alioth = cmodels.User.objects.get(email=alioth_login + UID_ALIOTH_DOMAIN)
                except cmodels.User.DoesNotExist:
                    continue

                if user_debian is None:
                    # Replace the @alioth uid with @debian uid

                    old_email = user_alioth.email
                    user_alioth.email = debian_login + "@debian.org"
                    log.info("%s: associate %s with %s", self.IDENTIFIER, old_email, user_alioth.email)
                    if not dry_run:
                        user_alioth.save()
                        user_alioth.add_log("converted from {} to {}".format(old_email, user_alioth.email))
                else:
                    # Merge foo@alioth into foo@debian

                    # Associate identifiers, previously associated to user@alioth,
                    # to user@debian
                    for ident in user_alioth.identifiers.all():
                        log.info("%s: move %s:%s to %s", self.IDENTIFIER, ident.type, ident.name, user_debian.email)
                        if not dry_run:
                            ident.add_log("reassigned from {} to {}".format(user_alioth.email, user_debian.email))
                            ident.user = user_debian
                            ident.save()

                    # Delete the user@alioth user
                    log.info("%s: delete user %s", self.IDENTIFIER, user_alioth.email)
                    if not dry_run:
                        user_debian.add_log("merged {} into {}".format(user_alioth.email, user_debian.email))
                        user_alioth.delete()


class FillFullnamesFromLDAP(hk.Task):
    """
    Fill empty full name fields with LDAP information
    """
    DEPENDS = [AssociateIdentities, UserAccountInfo]

    def __init__(self, *args, **kw):
        super(FillFullnamesFromLDAP, self).__init__(*args, **kw)
        self.fullnames_filled = 0

    def run_aggregation(self, stage):
        dry_run = self.hk.dry_run
        userinfo = self.hk.user_account_info.userinfo

        # Try to fill in missing full_name fields
        for u in cmodels.User.objects.filter(full_name=""):
            uinfo = userinfo.by_uid.get(u.email, None)
            if uinfo is None: continue
            # Skip UserInfo objects that do not know of a full name
            if not uinfo.fn: continue
            # FIXME: temporarily skip alioth full names with bad encodings,
            # until we figure out what is happening
            if "?" in uinfo.fn: continue
            if u.full_name != uinfo.fn:
                self.fullnames_filled += 1
                u.add_log("full name set to {} from Debian/Alioth account information".format(uinfo.fn))
                u.full_name = uinfo.fn
                if not dry_run:
                    u.save()

    def log_stats(self):
        log.info("%s: full_name fields filled: %d", self.IDENTIFIER, self.fullnames_filled)


class AggregateContributors(hk.Task):
    """
    Recompute the Aggregated tables
    """
    def run_aggregation(self, stage):
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()


class FixFullNames(hk.Task):
    """
    Clean User.full_name fields that contain only spaces, setting them to the
    empty string
    """
    def run_main(self, stage):
        dry_run = self.hk.dry_run

        for u in User.objects.all():
            if u.full_name.isspace():
                u.add_log("full name for {} was only made of spaces: setting it to the empty string".format(u.email))
                u.full_name = ""
                if not dry_run:
                    u.save()
