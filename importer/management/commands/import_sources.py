# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
from django.db import models
from contributors import models as cmodels
import sys
import logging
import json

log = logging.getLogger(__name__)

def read_from_fd(fd):
    res = json.load(fd)
    if isinstance(res, dict):
        return [res]
    else:
        return res

class Command(BaseCommand):
    help = 'Import data source information'
    def add_arguments(self, parser):
        parser.add_argument(
            '--quiet',
            action='store_true',
            dest='quiet',
            default=None,
            help='Disable progress reporting',
            )

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        if args:
            data = []
            for fname in args:
                with open(fname, "rt") as fd:
                    data.extend(read_from_fd(fd))
        else:
            data = read_from_fd(sys.stdin)

        cmodels.Source.import_json(data)
