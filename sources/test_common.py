from contributors import models as cmodels
from django.core.urlresolvers import reverse
from django.test import Client, override_settings
import re

# Deprecated test suite infrastructure, use dc/unittest.py instead

class DCTestClient(object):
    """
    Common bits for repetitive tests.

    Run a get or post queries, then check its results with a DCTestClientCheck
    instance.
    """
    def __init__(self, fixture, url_name, url_kwargs=None):
        if url_kwargs is None: url_kwargs = {}
        self.fixture = fixture
        self.c = Client()
        self.url_name = url_name
        self.url_kwargs = url_kwargs

    def setup_query(self, **kw):
        pass

    def assertQuery(self, expected_outcome, method, user=None, data=None, **kw):
        self.user = user

        if data is None:
            self.data = expected_outcome.get_default_data(self)
        else:
            self.data = data

        self.method = method

        self.setup_query(**kw)
        expected_outcome.setup_query(self, **kw)

        self.url = reverse(self.url_name, kwargs=self.url_kwargs)

        meth = getattr(self.c, method)
        if self.user is None:
            with override_settings(TEST_USER=None):
                self.response = meth(self.url, data=self.data)
        else:
            with override_settings(TEST_USER=self.user.email):
                self.response = meth(self.url, data=self.data)
        expected_outcome.check_result(self)

    def assertGet(self, expected_outcome, user=None, data=None, **kw):
        self.assertQuery(expected_outcome, "get", user, data, **kw)

    def assertPost(self, expected_outcome, user=None, data=None, **kw):
        self.assertQuery(expected_outcome, "post", user, data, **kw)


class DCTestClientCheck(object):
    def __init__(self):
        pass

    def get_default_data(self, tc):
        return {}

    def check_result(self, tc):
        tc.fixture.fail("check not implemented")

    def setup_query(self, tc, **kw):
        pass

class Success(DCTestClientCheck):
    def check_result(self, tc):
        if tc.response.status_code != 200:
            tc.fixture.fail("User {} got status code {} instead of a Success visiting {}".format(
                tc.user, tc.response.status_code, tc.url))

class MethodNotAllowed(DCTestClientCheck):
    def check_result(self, tc):
        if tc.response.status_code != 405:
            tc.fixture.fail("User {} got status code {} instead of a Method Not Allowed visiting {}".format(
                tc.user, tc.response.status_code, tc.url))

class BadMethod(DCTestClientCheck):
    def check_result(self, tc):
        if tc.response.status_code != 400:
            tc.fixture.fail("User {} got status code {} instead of a BadMethod visiting {}".format(
                tc.user, tc.response.status_code, tc.url))

class NotFound(DCTestClientCheck):
    def check_result(self, tc):
        if tc.response.status_code != 404:
            tc.fixture.fail("User {} got status code {} instead of a NotFound visiting {}".format(
                tc.user, tc.response.status_code, tc.url))

class Forbidden(DCTestClientCheck):
    def check_result(self, tc):
        if tc.response.status_code != 403:
            tc.fixture.fail("User {} got status code {} instead of a Forbidden visiting {}".format(
                tc.user, tc.response.status_code, tc.url))

class Redirect(DCTestClientCheck):
    def __init__(self, match=None):
        if match:
            self.match = re.compile(match)
            self.match_str = match
        else:
            self.match = None

    def check_result(self, tc):
        if tc.response.status_code != 302:
            tc.fixture.fail("User {} got status code {} instead of a Redirect visiting {}".format(
                tc.user, tc.response.status_code, tc.url))
        if self.match and not self.match.search(tc.response["Location"]):
            tc.fixture.fail("Response redirects to {} which does not match {}".format(
                tc.response["Location"], self.match_str))




class SimpleSourceFixtureMixin(object):
    def setUp(self):
        super(SimpleSourceFixtureMixin, self).setUp()

        self.source = cmodels.Source(name="test", desc="test source", url="http://www.example.org", auth_token="testsecret")
        self.source.save()

        self.ctype = cmodels.ContributionType(source=self.source, name="tester", desc="tester_desc", contrib_desc="tester_cdesc")
        self.ctype.save()

        self.user_admin = cmodels.User(email="enrico@debian.org", is_superuser=True)
        self.user_admin.save()
        self.user_admin.remote_user = "CONTRIBUTORS::DEBIAN:enrico"

        self.user_dd = cmodels.User(email="zobel@debian.org")
        self.user_dd.save()
        self.user_dd.remote_user = "CONTRIBUTORS::DEBIAN:zobel"

        self.user_dd1 = cmodels.User(email="92sam@debian.org")
        self.user_dd1.save()
        self.user_dd1.remote_user = "CONTRIBUTORS::DEBIAN:92sam"

        self.user_alioth = cmodels.User(email="safanaj-guest@users.alioth.debian.org")
        self.user_alioth.save()
        self.user_alioth.remote_user = "CONTRIBUTORS::DEBIAN:safanaj-guest@users.alioth.debian.org"

        self.user_alioth1 = cmodels.User(email="federico-guest@users.alioth.debian.org")
        self.user_alioth1.save()
        self.user_alioth1.remote_user = "CONTRIBUTORS::DEBIAN:federico-guest@users.alioth.debian.org"

        import re, datetime
        for u in "admin", "dd", "dd1", "alioth", "alioth1":
            # Build two IDs per user, one matching the user and one
            # NAME@example.org
            user = getattr(self, "user_{}".format(u))
            i1 = cmodels.Identifier.objects.create(type="email", name=user.email, user=user)
            setattr(self, "id_{}_user".format(u), i1)

            email = re.sub(r"(?:-guest)?@.+", "@example.org", user.email)
            i2 = cmodels.Identifier.objects.create(type="email", name=email, user=user)
            setattr(self, "id_{}_home".format(u), i2)

            # Make a contribution for each identifier
            dates = {
                "begin": datetime.date(2014, 2, 1),
                "until": datetime.date(2014, 2, 15),
            }
            c1 = cmodels.Contribution.objects.create(type=self.ctype, identifier=i1, **dates)
            setattr(self, "contrib_{}_user".format(u), c1)
            c2 = cmodels.Contribution.objects.create(type=self.ctype, identifier=i2, **dates)
            setattr(self, "contrib_{}_home".format(u), c2)

# Inspired from http://blog.liw.fi/posts/yarn/

class DCTestUtilsWhen(object):
    method = "get"
    url = None
    user = None
    data = None # Optional dict with GET or POST data

    def __init__(self, method=None, url=None, user=None, data=None, **kw):
        """
        Set up the parameters used by assertVisit to make a request
        """
        # Override the class defaults with the constructor arguments
        if method is not None: self.method = method
        if self.url is None or url is not None: self.url = url
        if self.user is None or user is not None: self.user = user
        if self.data is None or data is not None: self.data = data
        self.args = kw

    def setUp(self, fixture):
        """
        Normalise the parameters using data from the fixture
        """
        if isinstance(self.user, str):
            self.user = getattr(fixture, "user_{}".format(self.user))
        if self.data is None:
            self.data = {}

    def tearDown(self, fixture):
        pass

class DCTestUtilsThen(object):
    def __call__(self, fixture, response, when, test_client):
        fixture.fail("test not implemented")

    def __str__(self):
        return "user {} visits {}".format(self.user, self.url)

class ThenSuccess(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 200:
            fixture.fail("User {} got status code {} instead of a Success when {}".format(
                when.user, response.status_code, when))

class ThenForbidden(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 403:
            fixture.fail("User {} got status code {} instead of a Forbidden when {}".format(
                when.user, response.status_code, when))

class ThenMethodNotAllowed(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 405:
            fixture.fail("User {} got status code {} instead of a MethodNotAllowed when {}".format(
                when.user, response.status_code, when))

class ThenBadMethod(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 400:
            fixture.fail("User {} got status code {} instead of a BadMethod when {}".format(
                when.user, response.status_code, when))

class ThenNotFound(DCTestUtilsThen):
    def __call__(self, fixture, response, when, test_client):
        if response.status_code != 404:
            fixture.fail("User {} got status code {} instead of a NotFound when {}".format(
                when.user, response.status_code, when))

class DCTestUtilsMixin(object):
    def assertVisit(self, when, then, test_client=None):
        if test_client is None:
            test_client = Client()

        when.setUp(self)

        meth = getattr(test_client, when.method)
        if when.user is None:
            with override_settings(TEST_USER=None):
                response = meth(when.url, data=when.data)
        else:
            with override_settings(TEST_USER=when.user.email):
                response = meth(when.url, data=when.data)

        try:
            then(self, response, when, test_client)
        finally:
            when.tearDown(self)
