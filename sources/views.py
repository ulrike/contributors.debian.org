from django import http
from django.shortcuts import redirect, get_object_or_404
# from django.utils.translation import ugettext as _
from django.views.generic import DetailView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.exceptions import PermissionDenied
from django.http import StreamingHttpResponse
from wsgiref.util import FileWrapper
from django.forms import ModelForm
from dc.mixins import VisitorMixin
import contributors.models as cmodels
import logging
import os

log = logging.getLogger(__name__)


class SourcesList(VisitorMixin, TemplateView):
    template_name = "sources/list.html"

    def get_context_data(self, **kw):
        ctx = super(SourcesList, self).get_context_data(**kw)

        sources = []
        have_user = self.request.user.is_authenticated
        for s in cmodels.Source.objects.all().order_by("name"):
            if have_user:
                s.perm_can_admin = s.can_admin(self.visitor)
                s.perm_can_add_self = s.can_add_member(self.visitor, self.visitor)
            else:
                s.perm_can_admin = False
                s.perm_can_add_self = False
            sources.append(s)

        ctx["sources"] = sources

        return ctx


class SourceForm(ModelForm):
    class Meta:
        model = cmodels.Source
        exclude = ("last_import", "last_contribution", "admins",)


class VisitSourceMixin(VisitorMixin):
    def load_objects(self):
        super(VisitSourceMixin, self).load_objects()
        self.source = get_object_or_404(cmodels.Source, name=self.kwargs["sname"])

    def check_visitor_permission(self, perm):
        if perm == "source_admin":
            return self.source.can_admin(self.visitor)
        elif perm == "source_add_member":
            return self.source.can_add_member(self.visitor, self.visitor)
        else:
            return super(VisitSourceMixin, self).check_visitor_permission(perm)

    def get_context_data(self, **kw):
        ctx = super(VisitSourceMixin, self).get_context_data(**kw)
        ctx["source"] = self.source
        return ctx


class SourceView(VisitSourceMixin, TemplateView):
    template_name = "sources/view.html"

    def get_context_data(self, **kw):
        ctx = super(SourceView, self).get_context_data(**kw)
        ctx["ctypes"] = self.source.contribution_types.all().order_by("desc")
        ctx["contribs"] = cmodels.AggregatedPersonContribution.objects.filter(ctype__source=self.source).order_by("-until", "user__email")
        return ctx


class SourceMembers(VisitSourceMixin, TemplateView):
    template_name = "sources/members.html"
    require_visitor = "source_add_member"

    def get_context_data(self, **kw):
        ctx = super(SourceMembers, self).get_context_data(**kw)
        ctx["can_admin"] = self.source.can_admin(self.visitor)
        return ctx


class SourceMembersAdd(VisitSourceMixin, View):
    require_visitor = "source_add_member"

    def post(self, request, *args, **kw):
        user = get_object_or_404(cmodels.User, email=request.POST.get("name", None))
        if not self.source.can_add_member(request.user, user):
            raise PermissionDenied
        self.source.admins.add(user)
        return redirect("source_members", sname=self.source.name)


class SourceMembersDelete(VisitSourceMixin, View):
    require_visitor = "source_admin"

    def post(self, request, *args, **kw):
        user = get_object_or_404(cmodels.User, email=request.POST.get("name", None))
        self.source.admins.remove(user)
        return redirect("source_members", sname=self.source.name)


class CtypeView(VisitSourceMixin, TemplateView):
    template_name = "sources/cview.html"

    def get_context_data(self, **kw):
        ctx = super(CtypeView, self).get_context_data(**kw)
        ct = get_object_or_404(cmodels.ContributionType, source=self.source, name=self.kwargs["name"])
        ctx["ct"] = ct
        ctx["contribs"] = cmodels.AggregatedPersonContribution.objects.filter(ctype=ct) \
                                 .order_by("-until", "user__email"),
        return ctx


class SourceCreate(VisitorMixin, CreateView):
    template_name = "sources/source_form.html"
    model = cmodels.Source
    form_class = SourceForm
    success_url = reverse_lazy('source_list')

    def dispatch(self, request, *args, **kw):
        if not request.user.is_authenticated:
            raise PermissionDenied
        if not request.user.is_dd:
            raise PermissionDenied
        return super(SourceCreate, self).dispatch(request, *args, **kw)

    def form_valid(self, *args, **kw):
        res = super(SourceCreate, self).form_valid(*args, **kw)
        user = get_object_or_404(cmodels.User, pk=self.request.user.pk)
        self.object.admins.add(user)
        return res


class SourceMixin(VisitSourceMixin):
    model = cmodels.Source
    slug_field = "name"
    slug_url_kwarg = "sname"
    form_class = SourceForm
    success_url = reverse_lazy('source_list')
    require_visitor = "source_admin"

    def get_object(self, queryset=None):
        return self.source


class SourceUpdate(SourceMixin, UpdateView):
    template_name = "sources/source_form.html"


class SourceDelete(SourceMixin, DeleteView):
    template_name = "sources/source_confirm_delete.html"


class SourceBackupList(SourceMixin, DetailView):
    template_name = "sources/source_backup_list.html"

    def get_context_data(self, **kwargs):
        ctx = super(SourceBackupList, self).get_context_data(**kwargs)
        ctx["backups"] = list(self.object.backups())[::-1]
        return ctx


class SourceBackupDownload(SourceMixin, DetailView):
    def get(self, request, *args, **kwargs):
        source = self.get_object()
        backup_id = kwargs["backup_id"]
        backup_dir = source.get_backup_dir()
        if not backup_dir:
            raise http.Http404
        backup_file = os.path.join(backup_dir, backup_id + ".json.gz")
        if not os.path.exists(backup_file):
            raise http.Http404

        response = StreamingHttpResponse(FileWrapper(open(backup_file, "rb")),
                                         content_type="application/json")
        response["Content-Length"] = os.path.getsize(backup_file)
        response["Content-Encoding"] = "gzip"
        response["Content-Disposition"] = "attachment; filename={}-{}.json".format(
            backup_id, source.name)
        return response


class ContributionTypeForm(ModelForm):
    class Meta:
        model = cmodels.ContributionType
        exclude = ('source',)


class ContributionTypeMixin(SourceMixin):
    model = cmodels.ContributionType
    slug_field = "name"
    slug_url_kwarg = "name"
    form_class = ContributionTypeForm
    require_visitor = "source_admin"

    def load_objects(self):
        super(ContributionTypeMixin, self).load_objects()
        self.ctype = get_object_or_404(cmodels.ContributionType, source=self.source, name=self.kwargs["name"])

    def get_object(self):
        return self.ctype

    def get_queryset(self):
        qs = super(ContributionTypeMixin, self).get_queryset()
        qs = qs.filter(source=self.source)
        return qs

    def get_success_url(self):
        return reverse('source_update', kwargs={"sname": self.source.name})


class ContributionTypeCreate(SourceMixin, CreateView):
    template_name = "sources/contributiontype_form.html"
    model = cmodels.ContributionType
    form_class = ContributionTypeForm

    def get_success_url(self):
        return reverse('source_update', kwargs={"sname": self.source.name})

    def form_valid(self, form):
        form.instance.source = self.source
        return super(ContributionTypeCreate, self).form_valid(form)


class ContributionTypeUpdate(ContributionTypeMixin, UpdateView):
    template_name = "sources/contributiontype_form.html"


class ContributionTypeDelete(ContributionTypeMixin, DeleteView):
    template_name = "sources/contributiontype_confirm_delete.html"


class SourceDeleteContributions(SourceMixin, TemplateView):
    template_name = "sources/source_contributions_confirm_delete.html"
    require_visitor = "source_admin"

    def post(self, request, *args, **kwargs):
        # This implementation is "minimalist", not generic neither reusable.
        # Assume to inherit from SourceMixin or ContributionTypeMixin,
        # elsewise all is broken.

        for ct in self.source.contribution_types.all():
            ct.contributions.all().delete()
            cmodels.AggregatedPersonContribution.recompute(ctype=ct)
        cmodels.AggregatedSource.recompute(source=self.source)
        cmodels.AggregatedPerson.recompute()

        return redirect(self.source.get_absolute_url())


class ContributionTypeDeleteContributions(ContributionTypeMixin, DeleteView):
    template_name = "sources/contributiontype_contributions_confirm_delete.html"
    require_visitor = "source_admin"

    def post(self, request, *args, **kwargs):
        # This implementation is "minimalist", not generic neither reusable.
        # Assume to inherit from SourceMixin or ContributionTypeMixin,
        # elsewise all is broken.

        self.ctype.contributions.all().delete()
        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctype)
        cmodels.AggregatedSource.recompute(source=self.source)
        cmodels.AggregatedPerson.recompute()

        return redirect(self.source.get_absolute_url())


class SourceUserSettings(VisitSourceMixin, TemplateView):
    template_name = "sources/source_user_settings.html"
    require_visitor = "user"

    def post(self, request, *args, **kw):
        want_hidden = bool(request.POST.get("hide", False))
        settings = self.source.get_settings(self.visitor, create=want_hidden)
        if settings and settings.hidden != want_hidden:
            settings.hidden = want_hidden
            settings.save()
        for ctype in self.source.contribution_types.all():
            cmodels.AggregatedPersonContribution.recompute(user=self.visitor, ctype=ctype)
        cmodels.AggregatedSource.recompute(source=self.source)
        cmodels.AggregatedPerson.recompute(user=self.visitor)
        return redirect("source_view", sname=self.source.name)
