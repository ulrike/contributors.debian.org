from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models, transaction
from django.contrib.auth.models import BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.core.validators import RegexValidator
import datetime
import os
import gzip
import json


def _today():
    """
    Wrapper around datetime.date.today that can be mocked in unit tests
    """
    return datetime.date.today()


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **other_fields):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
            **other_fields
        )
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **other_fields):
        other_fields["is_superuser"] = True
        other_fields["is_staff"] = True
        return self.create_user(email, **other_fields)

    def lookup(self, name):
        if name.endswith("@debian"):
            name += ".org"
        elif name.endswith("@alioth"):
            name = name[:-6] + "users.alioth.debian.org"

        try:
            return User.objects.get(email=name)
        except User.DoesNotExist:
            return None


class User(PermissionsMixin, models.Model):
    objects = UserManager()

    # http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
    email = models.CharField(max_length=255, unique=True)
    full_name = models.CharField(max_length=255, blank=True)
    last_login = models.DateTimeField(_('last login'), default=timezone.now)
    is_staff = models.BooleanField(default=False)
    is_active = True
    # True if the person does not want any of their activity to be shown
    hidden = models.BooleanField(default=False)

    @property
    def is_dd(self):
        return self.email.endswith("@debian.org")

    def get_full_name(self):
        if self.full_name:
            return self.full_name
        return self.email

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.get_full_name()

    @property
    def unambiguous_user_id(self):
        try:
            local_part, domain = self.email.split("@", 1)
        except ValueError:
            return self.email
        if domain == "debian.org":
            return "{}@debian".format(local_part)
        elif domain == "users.alioth.debian.org":
            return "{}@alioth".format(local_part)
        else:
            return self.email

    @models.permalink
    def get_absolute_url(self):
        return ("contributor_detail", (), {"name": self.unambiguous_user_id})

    def get_nm_url(self):
        login, domain = self.email.split("@", 1)
        if domain == "debian.org":
            return "https://nm.debian.org/person/" + login
        else:
            return None

    def get_username(self):
        return self.email

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True

    def set_password(self, raw_password):
        pass

    def check_password(self, raw_password):
        return False

    def set_unusable_password(self):
        pass

    def has_usable_password(self):
        return False

    def add_log(self, text):
        """
        Create a new log entry for this identifier
        """
        entry = UserLog(user=self, text=text)
        entry.save()
        return entry

    def contributions(self):
        """
        Get a Contribution queryset with all contributions for all identifiers
        associated with this user
        """
        return Contribution.objects.filter(identifier__user=self)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []


class Identifier(models.Model):
    """
    One of the many ways identity is tracked in various bits of Debian.

    For example, debian login, alioth login, gpg key fingerprint, email
    address, wiki name.
    """
    # Type of identifier
    type = models.CharField(max_length=16, choices=(
        ("login", _("Debian/Alioth login")),
        ("fpr", _("OpenPGP key fingerprint")),
        ("email", _("Email address")),
        ("url", _("URL")),
        ("wiki", _("Wiki name")),
    ))
    # Identity name
    name = models.TextField(max_length=256)
    # If the owner of this identifier registed to log in with the contributor
    # site, we have a link to Django's User here.
    user = models.ForeignKey(User, null=True, related_name="identifiers")
    # True if the person does not want activity for this identifier to be shown
    hidden = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def add_log(self, text):
        """
        Create a new log entry for this identifier
        """
        entry = IdentifierLog(identifier=self, text=text)
        entry.save()
        return entry

    class Meta:
        unique_together = ('type', 'name')

    @classmethod
    def get_auto(cls, st):
        from debiancontributors.types import Identifier
        i = Identifier.create_auto(st)
        return cls.objects.get(type=i.type, name=i.id)


class Source(models.Model):
    """
    A data source, providing contribution informations.
    """
    # Machine-readable source name
    name = models.CharField(max_length=32, unique=True,
                            help_text=_("Data source name, shown on the website and"
                                        " used as a data source identifier when"
                                        " submitting contribution data."),
                            validators=[
                                RegexValidator(r"^[A-Za-z0-9][A-Za-z0-9._ -]*$",
                                               _("Name should start with a letter or number,"
                                                 " and can contain letters, numbers, dots,"
                                                 " spaces, underscores and minus signs")),
                            ])
    desc = models.TextField(verbose_name=_("Description"),
                            help_text=_("Long description shown on the website at the"
                                        " top of the data source page."))
    # Optional URL to a page about the source
    url = models.URLField(null=True, blank=True, max_length=255, verbose_name="URL",
                          help_text=_("Optional URL that links to the home page of the"
                                      " team that provides this data source."))
    # Authentication token used by the source when POSTing data
    auth_token = models.CharField(max_length=255, verbose_name=_("Authentication token"),
                                  help_text=_("Token that is sent alongside contribution"
                                              " information, to avoid random people"
                                              " trolling the site with garbage data."))
    # Implementation notes shown when configuring the source
    implementation_notes = models.TextField(blank=True,
                                            help_text=_("A description on how data mining is implemented"
                                                        " for this data source, that can be used by"
                                                        " people to fix issues if the primary maintainers"
                                                        " for it become unavailable."))
    # People who maintain this Source
    admins = models.ManyToManyField(User)
    # Time the last time data was imported into this source
    last_import = models.DateTimeField(null=True)
    # Time of the latest contributions seen from this source
    last_contribution = models.DateField(null=True)

    def __str__(self):
        return self.name

    def can_admin(self, user):
        """
        Check if the user is an admin for the given data source
        """
        if not user or not user.is_authenticated:
            return False
        if user.is_superuser:
            return True
        return self.admins.filter(id=user.id).exists()

    def can_add_member(self, user, new_member):
        """
        Check if user can add new_member to this source
        """
        if not user or not user.is_authenticated:
            return False
        if user.is_superuser:
            return True
        if self.admins.filter(id=user.id).exists():
            return True
        if not user.is_dd:
            return False
        return user.id == new_member.id

    def get_settings(self, user, create=False):
        """
        Get the SourceSettings objec for this user and source
        """
        try:
            return UserSourceSettings.objects.get(source=self, user=user)
        except UserSourceSettings.DoesNotExist:
            if create:
                return UserSourceSettings.objects.create(source=self, user=user)
            else:
                return None

    @models.permalink
    def get_absolute_url(self):
        return ("source_view", (), {"sname": self.name})

    @classmethod
    def import_json(cls, data):
        from importer.importer import SourceImporter
        si = SourceImporter()
        si.import_sources(data)

    @classmethod
    def export_json(cls, names=None, with_tokens=False):
        """
        Export Source and ContributionType information into a JSON-able array
        """
        if names:
            sources = cls.objects.find(name__in=names)
        else:
            sources = cls.objects.all()

        fake_tok = 1
        res = []
        for s in sources:
            if with_tokens:
                tok = s.auth_token
            else:
                tok = "token_%d" % fake_tok
                fake_tok += 1

            ctypes = []

            js = {
                "name": s.name,
                "desc": s.desc,
                "url": s.url,
                "auth_token": tok,
                "contribution_types": ctypes,
            }

            for ct in s.contribution_types.all():
                jct = {
                    "name": ct.name,
                    "desc": ct.desc,
                    "contrib_desc": ct.contrib_desc,
                }
                ctypes.append(jct)

            res.append(js)

        return res

    def get_backup_dir(self, create=False):
        """
        Return the full pathname to the backup directory for this data source.

        Returns None if no DATA_DIR is configured.
        """
        data_dir = getattr(settings, "DATA_DIR", None)
        if data_dir is None:
            return None

        backupdir = os.path.join(data_dir, "contrib-backups", self.name)

        # Create the output directory if it's missing
        if not os.path.exists(backupdir):
            if create:
                os.makedirs(backupdir)
            else:
                return None

        return backupdir

    def get_submission_log_dir(self, create=False):
        """
        Return the full pathname to the submission log directory for this data source.

        Returns None if no DATA_DIR is configured.
        """
        data_dir = getattr(settings, "DATA_DIR", None)
        if data_dir is None:
            return None

        logdir = os.path.join(data_dir, "submissions", self.name)

        # Create the output directory if it's missing
        if not os.path.exists(logdir):
            if create:
                os.makedirs(logdir)
            else:
                return None

        return logdir

    def submission_log(self):
        """
        Generate a sequence of (pathname, datetime) for each submission backup
        found in the backup directory
        """
        root = self.get_submission_log_dir()
        if root is None:
            return []
        res = []
        for fn in os.listdir(root):
            try:
                dt = datetime.datetime.strptime(fn, "%Y%m%d-%H:%M:%S.json.gz")
            except ValueError:
                pass
            res.append((os.path.join(root, fn), dt))
        res.sort(key=lambda x: x[1])
        return res

    def log_submission(self, request, data, result, submission_time=None):
        """
        Log a data submission
        """
        # Skip backups if we don't have a backup dir
        logdir = self.get_submission_log_dir(create=True)
        if logdir is None:
            return

        # Create a time-based tag to use as a prefix for the log file
        if submission_time is None:
            submission_time = datetime.datetime.utcnow()
        ts_tag = submission_time.strftime("%Y%m%d-%H:%M:%S")

        # All the information we want to log
        payload = {
            "request": {
                "meta": {k: str(v) for k, v in request.META.items()},
            },
            "data": data,
            "result": result,
        }

        # Write the contributions log
        outfile = os.path.join(logdir, "{}.json.gz".format(ts_tag))
        with gzip.open(outfile, "wt") as fd:
            json.dump(payload, fd, indent=1)

    def make_backup(self, backup_time=None):
        """
        Make a backup of the contributions in this data source
        """
        import debiancontributors as dc

        # Skip backups if we don't have a backup dir
        backupdir = self.get_backup_dir(create=True)
        if backupdir is None:
            return

        # Create a time-based tag to use as a prefix for the backup file
        if backup_time is None:
            backup_time = datetime.datetime.utcnow()
        ts_tag = backup_time.strftime("%Y%m%d")

        # Create a Submission with all the data for this source
        out = dc.Submission(self.name)
        contrib_types = set()
        contrib_count = 0
        for c in Contribution.objects.filter(type__source=self).select_related("type", "identifier", "identifier__user"):
            ident = dc.Identifier(c.identifier.type, c.identifier.name, c.identifier.user.full_name if c.identifier.user else None)
            contrib = dc.Contribution(c.type.name, c.begin, c.until, c.url)
            out.add_contribution(ident, contrib)
            contrib_count += 1
            contrib_types.add(c.type.name)

        # Write the contributions backup
        outfile = os.path.join(backupdir, "{}.json.gz".format(ts_tag))
        with gzip.open(outfile, "wt") as fd:
            out.to_json(fd, indent=1)

        infofile = os.path.join(backupdir, "{}.json.info".format(ts_tag))
        with open(infofile, "wt") as fd:
            return json.dump({
                "backup_time": backup_time.strftime("%Y-%m-%d %H:%M:%S"),
                "ident_count": len(out.entries),
                "contrib_count": contrib_count,
                "contrib_types": sorted(contrib_types),
            }, fd, indent=1)

    def backups(self):
        """
        Generate a sequence of backup files and their info, sorted by backup time
        """
        backupdir = self.get_backup_dir()
        if backupdir is None:
            return

        for fname in sorted(os.listdir(backupdir)):
            # Only look at info files
            if not fname.endswith(".json.info"):
                continue
            dt_prefix = fname[:8]

            # Ensure that the file name begins with a valid date
            try:
                dt = datetime.datetime.strptime(dt_prefix, "%Y%m%d")
            except ValueError:
                continue

            # Ensure that we also have a data file
            datafile = os.path.join(backupdir, dt_prefix + ".json.gz")
            if not os.path.exists(datafile):
                continue

            # Load the backup information
            infofile = os.path.join(backupdir, fname)
            with open(infofile, "rt") as fd:
                try:
                    info = json.load(fd)
                except ValueError:
                    continue

            info["id"] = dt_prefix
            info["datafile"] = datafile
            info["infofile"] = infofile
            info["date"] = dt.date()

            yield info


class UserSourceSettings(models.Model):
    """
    User settings for a data source
    """
    # Identifier for which we are holding settings
    user = models.ForeignKey(User, related_name="source_settings")
    # Contribution type the person wants to control
    source = models.ForeignKey(Source, related_name="user_settings")
    # Do not show this kind of contributions for this identifier
    hidden = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'source')


class ContributionType(models.Model):
    """
    A contribution type for a data source
    """
    # Data source that manages this contribution type
    source = models.ForeignKey(Source, related_name="contribution_types")
    # Machine readable contribution type
    name = models.CharField(max_length=32,
                            help_text=_("Contribution type name, shown on the website"
                                        " and used as an identifier when submitting"
                                        " contribution data."),
                            validators=[
                                RegexValidator(r"^[A-Za-z0-9][A-Za-z0-9._ -]*$",
                                               _("Name should start with a letter or number,"
                                                 " and can contain letters, numbers, dots,"
                                                 " spaces, underscores and minus signs")),
                            ])
    # Human readable contribution type description
    desc = models.TextField(verbose_name=_("Contribution description"),
                            help_text=_("Description of this type of contribution."
                                        " For example: 'wiki editing'"))
    # Description for the kind of contribution
    contrib_desc = models.TextField(verbose_name=_("Contributor description"),
                                    help_text=_("Description o a person who does"
                                                " contributions of this type."
                                                " For example: 'wiki editor'."))

    def __str__(self):
        return "%s:%s" % (self.source.name, self.name)

    @models.permalink
    def get_absolute_url(self):
        return ("source_ctype_view", (), {"sname": self.source.name, "name": self.name})

    class Meta:
        unique_together = ('source', 'name')


class Contribution(models.Model):
    """
    Description of contributions by one identifier in a source+type
    """
    # Type of contribution
    type = models.ForeignKey(ContributionType, related_name="contributions")
    # Identifier used for this contribution
    identifier = models.ForeignKey(Identifier, related_name="contributions")
    # When the first contribution was found
    begin = models.DateField()
    # When the last contribution was found
    until = models.DateField()
    # Optional pointer to some team-specific info page
    url = models.URLField(null=True, blank=True, max_length=255)

    class Meta:
        unique_together = ('identifier', 'type')

    @property
    def hidden(self):
        if self.identifier.hidden:
            return True
        if self.identifier.user.hidden:
            return True
        try:
            settings = UserSourceSettings.objects.get(user=self.identifier.user, source=self.type.source)
        except UserSourceSettings.DoesNotExist:
            settings = None
        if settings and settings.hidden:
            return True
        return False

    def save(self, *args, **kw):
        """
        Make sure we always fill begin and until, defaulting to today
        """
        if self.begin is None:
            if self.until is None:
                self.begin = _today()
            else:
                self.begin = self.until
        if self.until is None:
            self.until = _today()
        super(Contribution, self).save(*args, **kw)

    def extend(self, begin, until):
        """
        Extend the begin-until date range with a new date range.

        If begin is missing, defaults to the previous begin value.
        If no begin value was ever set, default to today.

        If until is missing, default to today.

        Returns True if the date range was changed, else False.
        """
        changed = False

        # If missing, defaults to previous 'begin' value.
        if begin is None:
            begin = self.begin
        # If never seen, defaults to 'today'
        if begin is None:
            # If a data source only reports 'until', we have a better default
            # than 'today'
            if until is not None:
                begin = until
            else:
                begin = _today()
        if self.begin > begin:
            self.begin = begin
            changed = True

        # If missing, defaults to 'today'
        if until is None:
            until = _today()
        if self.until < until:
            self.until = until
            changed = True

        return changed

    def replace(self, begin, until):
        """
        Extend the begin-until date range with a new date range.

        If begin is missing, defaults to the previous begin value.
        If no begin value was ever set, default to today.

        If until is missing, default to today.

        Returns True if the date range was changed, else False.
        """
        changed = False

        # If missing, defaults to previous 'begin' value.
        if begin is None:
            begin = self.begin
        # If never seen, defaults to 'today'
        if begin is None:
            # If a data source only reports 'until', we have a better default
            # than 'today'
            if until is not None:
                begin = until
            else:
                begin = _today()
        if self.begin != begin:
            self.begin = begin
            changed = True

        # If missing, defaults to 'today'
        if until is None:
            until = _today()
        if self.until != until:
            self.until = until
            changed = True

        return changed


class UserLog(models.Model):
    """
    Log of actions performed on a user
    """
    user = models.ForeignKey(User, related_name="log")
    ts = models.DateTimeField(auto_now_add=True, verbose_name="timestamp")
    text = models.TextField()


class IdentifierLog(models.Model):
    """
    Log of actions performed on an identifier
    """
    identifier = models.ForeignKey(Identifier, related_name="log")
    ts = models.DateTimeField(auto_now_add=True, verbose_name="timestamp")
    text = models.TextField()


class AggregatedSource(models.Model):
    """
    Contributions aggregated by Source
    """
    source = models.OneToOneField(Source, related_name="aggregated_source")
    begin = models.DateField()
    until = models.DateField()

    @classmethod
    def recompute(cls, source=None):
        from django.db.models import Min, Max

        with transaction.atomic():
            # We aggregate based on AggregatedPersonContribution which is already
            # visibility-filtered, so we do not need to check hidden flags here

            # Delete old records
            to_delete = cls.objects.all()
            if source:
                to_delete = to_delete.filter(source=source)
            to_delete.delete()

            # Aggregate by user
            sources = Source.objects.all()
            if source:
                sources = sources.filter(pk=source.pk)
            for s in sources \
                    .annotate(begin=Min("contribution_types__aggregated_contributions__begin")) \
                    .annotate(until=Max("contribution_types__aggregated_contributions__until")):
                if s.begin is None:
                    continue
                if s.until is None:
                    continue
                # Insert the new record
                cls.objects.create(source=s, begin=s.begin, until=s.until)


class AggregatedPerson(models.Model):
    """
    Contributions aggregated by person
    """
    user = models.OneToOneField(User, related_name="aggregated_person")
    begin = models.DateField()
    until = models.DateField()

    @classmethod
    def recompute(cls, user=None):
        from django.db.models import Min, Max

        # We aggregate based on AggregatedPersonContribution which is already
        # visibility-filtered, so we do not need to check hidden flags here

        with transaction.atomic():
            # Delete old records
            to_delete = cls.objects.all()
            if user:
                to_delete = to_delete.filter(user=user)
            to_delete.delete()

            # Aggregate by user
            users = User.objects.all()
            if user:
                users = users.filter(pk=user.pk)
            for u in users \
                    .annotate(begin=Min("aggregated_contributions__begin")) \
                    .annotate(until=Max("aggregated_contributions__until")):
                if u.begin is None:
                    continue
                if u.until is None:
                    continue
                # Insert the new record
                cls.objects.create(user=u, begin=u.begin, until=u.until)


class AggregatedPersonContribution(models.Model):
    """
    Contributions aggregated by person and contribution type
    """
    user = models.ForeignKey(User, related_name="aggregated_contributions")
    ctype = models.ForeignKey(ContributionType, related_name="aggregated_contributions")
    begin = models.DateField()
    until = models.DateField()

    class Meta:
        unique_together = ('user', 'ctype')

    @classmethod
    def recompute(cls, user=None, ctype=None):
        with transaction.atomic():
            # Recompute
            to_aggregate = Contribution.objects.select_related("identifier").exclude(identifier__user=None)
            if ctype:
                to_aggregate = to_aggregate.filter(type=ctype)
            if user:
                to_aggregate = to_aggregate.filter(identifier__user=user)

            # Skip if user is hidden
            to_aggregate = to_aggregate.exclude(identifier__user__hidden=True)

            # Skip if identifier is hidden
            to_aggregate = to_aggregate.exclude(identifier__hidden=True)

            # Build a blacklist with the hidden (user, source) pairs
            hidden_settings = UserSourceSettings.objects.filter(hidden=True)
            if ctype:
                hidden_settings = hidden_settings.filter(source=ctype.source)
            if user:
                hidden_settings = hidden_settings.filter(user=user)
            hidden = set((s.user_id, s.source_id) for s in hidden_settings)

            # Aggregate by user and ctype
            aggregated = {}
            for c in to_aggregate.select_related("identifier", "type"):
                # Skip if (source, user) is hidden
                if (c.identifier.user_id, c.type.source_id) in hidden:
                    continue

                key = (c.identifier.user_id, c.type_id)
                old = aggregated.get(key, None)
                if old is None:
                    aggregated[key] = (c.begin, c.until)
                else:
                    aggregated[key] = (min(c.begin, old[0]), max(c.until, old[1]))

            # Delete old records
            to_delete = cls.objects.all()
            if user:
                to_delete = to_delete.filter(user=user)
            if ctype:
                to_delete = to_delete.filter(ctype=ctype)
            to_delete.delete()

            # Insert the new records
            for (uid, ctypeid), (begin, until) in aggregated.items():
                cls.objects.create(user_id=uid, ctype_id=ctypeid, begin=begin, until=until)
