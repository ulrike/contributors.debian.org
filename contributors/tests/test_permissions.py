from django.test import TestCase
from dc.unittest import SourceFixtureMixin
from django.core.urlresolvers import reverse
from contributors import models as cmodels
import json


class TestContributorsViews(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_contributors_success, user)
            cls._add_method(cls._test_contributors_flat_success, user)
            cls._add_method(cls._test_contributors_new_success, user)
            cls._add_method(cls._test_site_status_success, user)

    def _test_contributors_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors"))
            self.assertContains(response, "This is a list of all the ")

    def _test_contributors_flat_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_flat"))
            self.assertContains(response, "This is a list of all the ")

    def _test_contributors_new_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_new"))
            self.assertContains(response, "This is a list of all the ")

    def _test_site_status_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("site_status"))
            self.assertContains(response, "Site status")


class TestExportSources(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        cls._add_method(cls._test_can_see_tokens, "admin")
        for user in "dd", "dd1", "alioth", "alioth1":
            cls._add_method(cls._test_cannot_see_tokens, user)
            # Even members of a data source cannot see tokens in export, not even
            # in the sources they admin, to avoid accidentally leaking tokens when
            # giving an export to new developers.
            cls._add_method(cls._test_cannot_see_tokens, user, add_member=True)
        cls._add_method(cls._test_cannot_see_tokens, None)

    def _test_can_see_tokens(self, user, add_member=False):
        if add_member:
            self.sources.test.admins.add(self.users[user])
        with self.login(user):
            response = self.client.get(reverse("contributors_export_sources"))
            data = response.json()
            source = [x for x in data if x["name"] == "test"][0]
            self.assertIsNotNone(source)
            self.assertEqual(source["auth_token"], "testsecret")

    def _test_cannot_see_tokens(self, user, add_member=False):
        if add_member:
            self.sources.test.admins.add(self.users[user])
        with self.login(user):
            response = self.client.get(reverse("contributors_export_sources"))
            data = response.json()
            source = [x for x in data if x["name"] == "test"][0]
            self.assertIsNotNone(source)
            self.assertNotEqual(source["auth_token"], "testsecret")


class TestPost(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_success, user)
            cls._add_method(cls._test_wrong_token, user)
            cls._add_method(cls._test_no_get, user)

    def _get_submission(self, token):
        from django.core.files.base import ContentFile
        submission = [{
            "id": [{"type": "login", "id": "enrico"}],
            "contributions": [{"type": "tester"}]
        }, ]
        return {"source": "test", "auth_token": token, "data": ContentFile(json.dumps(submission), "foo.json")}

    def _test_success(self, user):
        with self.login(user):
            response = self.client.post(reverse("contributors_post"), data=self._get_submission("testsecret"))
            self.assertEqual(response.status_code, 200)
            data = response.json()
            self.assertEqual(data["code"], 200)
            self.assertEqual(data["records_parsed"], 1)
            self.assertEqual(data["contributions_processed"], 1)

    def _test_wrong_token(self, user):
        with self.login(user):
            response = self.client.post(reverse("contributors_post"), data=self._get_submission("testsecret1"))
            self.assertEqual(response.status_code, 403)
            data = response.json()
            self.assertEqual(data["code"], 403)
            self.assertEqual(data["records_parsed"], 0)
            self.assertEqual(data["contributions_processed"], 0)
        pass

    def _test_no_get(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_post"))
            self.assertEqual(response.status_code, 405)


class TestClaim(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_claim_success, user)
            cls._add_method(cls._test_claim_idents_success, user)
            cls._add_method(cls._test_claim_people_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_claim_forbidden, user)
            cls._add_method(cls._test_claim_idents_forbidden, user)
            cls._add_method(cls._test_claim_people_forbidden, user)

    def _test_claim_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim"))
            self.assertEqual(response.status_code, 200)
            response = self.client.post(reverse("contributors_claim"), data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})
            self.assertEqual(response.status_code, 200)

    def _test_claim_forbidden(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("contributors_claim"), data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})
            self.assertEqual(response.status_code, 403)

    def _test_claim_idents_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim_idents", kwargs={"type": "email"}))
            self.assertEqual(response.status_code, 200)

    def _test_claim_idents_forbidden(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim_idents", kwargs={"type": "email"}))
            self.assertEqual(response.status_code, 403)

    def _test_claim_people_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim_people"))
            self.assertEqual(response.status_code, 200)

    def _test_claim_people_forbidden(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_claim_people"))
            self.assertEqual(response.status_code, 403)


class MIATestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_success, user)

    def _test_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia"))
            self.assertContains(response, "Debian Developers Missing In Action")


class MIAQueryTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_forbidden, user)

    def _test_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia_query"))
            self.assertContains(response, "Query contributors by identifier")

    def _test_forbidden(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia_query"))
            self.assertPermissionDenied(response)


class APITestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(APITestCase, cls).setUpClass()
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()

    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_forbidden, user)

    def _test_success(self, user):
        client = self.make_test_apiclient(user)
        response = client.get(reverse('contributors-list'), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 5)

    def _test_forbidden(self, user):
        client = self.make_test_apiclient(user)
        response = client.get(reverse('contributors-list'))
        self.assertPermissionDenied(response)
