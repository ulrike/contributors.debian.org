# Django settings for dc project.
from django.conf import global_settings
import os.path

DEBUG = True

ADMINS = (
    ('Debian New Member Frontdesk', 'nm@debian.org'),
)
MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = "nm@debian.org"
SERVER_EMAIL = "nm@debian.org"

AUTH_USER_MODEL = 'contributors.User'

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'i53l+g4&fs4b+lxdoj!m2t$3axfxryat$^i$5es&t5wpc2lz#a'

MIDDLEWARE_CLASSES = (
    'dc.auth.AuthMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
    # Uncomment to authenticate via REMOTE_USER supplied by Apache
    'django.contrib.auth.backends.RemoteUserBackend',
)

ROOT_URLCONF = 'dc.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'dc.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    # https://github.com/spanezz/django-housekeeping
    'django_housekeeping',
    'deblayout',
    'dclayout',
    'contributor',
    'contributors',
    'sources',
    'importer',
    'carnivore',
    'mia',
    'rest_framework',
    'django_filters',
    'deploy',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                "django.template.context_processors.request",
            ],
        },
    },
]

# New 1.7 test runner, we set it explicitly to silence django's checks
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
DATA_DIR = os.path.join(PROJECT_DIR, 'data')

LDAP_DEBIAN_URI = "ldap://db.debian.org"
LDAP_ALIOTH_URI = "ldaps://alioth.debian.org"

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    #'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'DEFAULT_PAGINATION_CLASS': 'contributors.pagination.LimitOffsetPaginationWithUnlimited',
    'PAGE_SIZE': 25,
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '%s/db-used-for-development.sqlite' % DATA_DIR, # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Try importing local settings from local_settings.py, if we can't, it's just
# fine, use defaults from this file
try:
    from dc.local_settings import *
except:
    pass


# Log to file on top of the normal django logging
if not DEBUG:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            },
        },
        'formatters': {
            'logfile': {
                'format': '[%(asctime)s %(module)s] %(levelname)s: %(message)s',
            },
        },
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '/srv/contributors.debian.org/dc/data/logs/django.log',
                'filters': ['require_debug_false'],
                'formatter': 'logfile',
            },
        },
        'loggers': {
            'django.request': {
                'handlers': ['file'],
                'level': 'INFO',
                'propagate': True,
            },
            'django.db.backends': {
                'level': 'INFO',
                'handlers': ['file'],
                'propagate': True,
            },
            '': {
                'level': 'INFO',
                'handlers': ['file'],
                'propagate': True,
            },
        },
    }
