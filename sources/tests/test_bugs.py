"""
Regression tests for bugs
"""
from django.test import TestCase
from sources.test_common import *

class TestContributionTypeUpdate(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def setUp(self):
        super(TestContributionTypeUpdate, self).setUp()
        self.source1 = cmodels.Source(name="test1", desc="test1 source", url="http://www.example.org", auth_token="testsecret")
        self.source1.save()
        self.ctype1 = cmodels.ContributionType(source=self.source1, name="tester", desc="tester_desc", contrib_desc="tester_cdesc")
        self.ctype1.save()

    def test_same_name(self):
        class WhenVisit(DCTestUtilsWhen):
            url = reverse("source_ctype_update", kwargs={"sname": "test", "name": "tester"})
        self.assertVisit(WhenVisit(user=self.user_admin), ThenSuccess())
