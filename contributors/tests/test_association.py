from django_housekeeping import Housekeeping
from django.test import TestCase
import contributors.models as cmodels
from django.test.utils import override_settings
from sources.test_common import *
from contributors.housekeeping import UserInfo
import re
import sys
import logging

logging.getLogger("django_housekeeping.run").addHandler(logging.StreamHandler(sys.stderr))


class HousekeepingTest(object):
    def run_housekeeping(self, match="", mock_data=[]):
        # Run AssociateLDAPEmails
        hk = Housekeeping(dry_run=False)
        hk.autodiscover()
        hk.init()
        hk.user_account_info.mock_data = mock_data
        hk.run(run_filter=lambda name: re.search(match, name))


class TestAssociateEmails(HousekeepingTest, TestCase):
    def run_housekeeping(self, mock_data=[]):
        super(TestAssociateEmails, self).run_housekeeping(
            match="UserAccountInfo|AssociateIdentities|AssociateLDAPEmails",
            mock_data=mock_data)

    @override_settings(DEVEL_SYSTEM=True)
    def testAliothEmail(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="email", name="enrico-guest@users.alioth.debian.org")

        self.run_housekeeping()

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        self.assertEqual(ident.user.email, "enrico-guest@users.alioth.debian.org")

    @override_settings(DEVEL_SYSTEM=True)
    def testDebianEmail(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="email", name="enrico@debian.org")

        self.run_housekeeping()

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        self.assertEqual(ident.user.email, "enrico@debian.org")

    @override_settings(DEVEL_SYSTEM=True)
    def testOtherEmail(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="email", name="test@example.org")

        self.run_housekeeping(mock_data=[
            UserInfo("test@debian.org", emails=["test@example.org"]),
        ])

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        # Ensure that it did not get associated
        self.assertEqual(ident.user.email, "test@debian.org")

    @override_settings(DEVEL_SYSTEM=True)
    def testMissingEmail(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="email", name="enrico@example.org")

        self.run_housekeeping()

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        # Ensure that it did not get associated
        self.assertEqual(ident.user, None)


class TestAssociateLogins(HousekeepingTest, TestCase):
    def run_housekeeping(self):
        super(TestAssociateLogins, self).run_housekeeping(
            match="UserAccountInfo|AssociateIdentities|AssociateLDAPLogins")

    @override_settings(DEVEL_SYSTEM=True)
    def testAliothLogin(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="login", name="enrico-guest")

        self.run_housekeeping()

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        self.assertEqual(ident.user.email, "enrico-guest@users.alioth.debian.org")

    @override_settings(DEVEL_SYSTEM=True)
    def testDebianLogin(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="login", name="enrico")

        self.run_housekeeping()

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        self.assertEqual(ident.user.email, "enrico@debian.org")


class TestAssociateFingerprints(HousekeepingTest, TestCase):
    def run_housekeeping(self, mock_data=[]):
        super(TestAssociateFingerprints, self).run_housekeeping(
            match="UserAccountInfo|AssociateIdentities|AssociateLDAPFingerprints",
            mock_data=mock_data)

    @override_settings(DEVEL_SYSTEM=True)
    def testDebianLogin(self):
        # Create an email identifier
        ident = cmodels.Identifier.objects.create(type="fpr", name="1793D6AB75663E6BF104953A634F4BD1E7AD5568")

        self.run_housekeeping(mock_data = [
            UserInfo(uid="enrico@debian.org", fprs=["1793D6AB75663E6BF104953A634F4BD1E7AD5568"]),
        ])

        # Reload the identifier from the database to see if it got associated
        ident = cmodels.Identifier.objects.get(pk=ident.pk)

        self.assertEqual(ident.user.email, "enrico@debian.org")


class TestUserMerging(HousekeepingTest, TestCase):
    @override_settings(DEVEL_SYSTEM=True)
    def testMergeSameName(self):
        # Create an email identifier
        ident_alioth = cmodels.Identifier.objects.create(type="email", name="enrico@users.alioth.debian.org")
        ident_alioth.user = cmodels.User.objects.create_user("enrico@users.alioth.debian.org")
        ident_alioth.save()
        ident_debian = cmodels.Identifier.objects.create(type="email", name="enrico@debian.org")
        ident_debian.user = cmodels.User.objects.create_user("enrico@debian.org")
        ident_debian.save()

        self.run_housekeeping(match=r"UserAccountInfo|MergeDebianAliothUsers")

        # The alioth user should have disappeared
        self.assertEqual(cmodels.User.objects.filter(email="enrico@users.alioth.debian.org").count(), 0)

        # The Debian user is still there
        user_debian = cmodels.User.objects.get(email="enrico@debian.org")

        # Reload the identifier from the database to see if it got associated
        ident_alioth = cmodels.Identifier.objects.get(pk=ident_alioth.pk)
        self.assertEqual(ident_alioth.user.pk, user_debian.pk)
        self.assertEqual(ident_debian.user.pk, user_debian.pk)

    @override_settings(DEVEL_SYSTEM=True)
    def testMergeForwardEmail(self):
        # Create an email identifier
        ident_alioth = cmodels.Identifier.objects.create(type="email", name="foo-guest@users.alioth.debian.org")
        ident_alioth.user = cmodels.User.objects.create_user("foo-guest@users.alioth.debian.org")
        ident_alioth.save()
        ident_debian = cmodels.Identifier.objects.create(type="email", name="enrico@debian.org")
        ident_debian.user = cmodels.User.objects.create_user("enrico@debian.org")
        ident_debian.save()

        self.run_housekeeping(match=r"UserAccountInfo|MergeDebianAliothUsers",
                              mock_data=[
                                  UserInfo("enrico@debian.org", emails=["test@example.org"]),
                                  UserInfo("foo-guest@users.alioth.debian.org", emails=["test@example.org"]),
                              ])

        # The alioth user should have disappeared
        self.assertEqual(cmodels.User.objects.filter(email="foo-guest@users.alioth.debian.org").count(), 0)

        # The Debian user is still there
        user_debian = cmodels.User.objects.get(email="enrico@debian.org")

        # Reload the identifier from the database to see if it got associated
        ident_alioth = cmodels.Identifier.objects.get(pk=ident_alioth.pk)
        self.assertEqual(ident_alioth.user.pk, user_debian.pk)
        self.assertEqual(ident_debian.user.pk, user_debian.pk)


class TestFillFullnames(HousekeepingTest, TestCase):
    @override_settings(DEVEL_SYSTEM=True)
    def testFill(self):
        user_debian = cmodels.User.objects.create_user("enrico@debian.org")
        user_alioth = cmodels.User.objects.create_user("enrico-guest@users.alioth.debian.org")

        self.run_housekeeping(match=r"AssociateIdentities|UserAccountInfo|FillFullnamesFromLDAP",
                              mock_data=[
                                  UserInfo(uid="enrico@debian.org", fn="Enrico Zini"),
                                  UserInfo(uid="enrico-guest@users.alioth.debian.org", fn="Zini Enrico"),
                              ])

        # Reload the users from the DB
        user_debian = cmodels.User.objects.get(pk=user_debian.pk)
        user_alioth = cmodels.User.objects.get(pk=user_alioth.pk)

        # Verify that the full name fields have been filled
        self.assertEqual(user_debian.full_name, "Enrico Zini")
        self.assertEqual(user_alioth.full_name, "Zini Enrico")

