from django.core.exceptions import PermissionDenied
import contributors.models as cmodels
import six

class VisitorMixin(object):
    """
    Add self.visitor and self.impersonator to the View for the person visiting
    the site
    """
    # Define as "user", "dd", or "admin" to raise PermissionDenied if the given
    # test on the visitor fails
    require_visitor = None

    def set_visitor_info(self):
        self.impersonator = None

        if not self.request.user.is_authenticated:
            self.visitor = None
        else:
            self.visitor = self.request.user

            # Implement impersonation if requested in session
            if self.visitor.is_superuser:
                email = self.request.session.get("impersonate", None)
                if email is not None:
                    p = cmodels.User.objects.get(email=email)
                    if p is not None:
                        self.impersonator = self.visitor
                        self.visitor = p

    def load_objects(self):
        """
        Hook to set self.* members from request parameters, so that they are
        available to the rest of the view members.
        """
        self.set_visitor_info()

    def check_visitor_permission(self, perm):
        """
        Check the visitor against self.require_visitor
        """
        if self.require_visitor == "user":
            return True
        elif self.require_visitor == "dd":
            return self.visitor.is_dd
        elif self.require_visitor == "admin":
            return self.visitor.is_superuser
        else:
            return False

    def check_permissions(self):
        """
        Raise PermissionDenied if some of the permissions requested by the view
        configuration are not met.

        Subclasses can extend this to check their own permissions.
        """
        if self.visitor and self.visitor.is_superuser: return

        if self.require_visitor:
            if self.visitor is None: raise PermissionDenied
            if isinstance(self.require_visitor, six.string_types):
                if not self.check_visitor_permission(self.require_visitor):
                    raise PermissionDenied
            else:
                if not any(self.check_visitor_permission(x) for x in self.require_visitor):
                    raise PermissionDenied

    def dispatch(self, request, *args, **kwargs):
        self.load_objects()
        self.check_permissions()
        return super(VisitorMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kw):
        ctx = super(VisitorMixin, self).get_context_data(**kw)
        ctx["visitor"] = self.visitor
        ctx["impersonator"] = self.impersonator
        return ctx
