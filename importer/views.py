# from django.utils.translation import ugettext as _
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django import http
from dc.mixins import VisitorMixin
from .importer import Importer
import contributors.models as cmodels
import json


class CSRFExemptMixin:
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSRFExemptMixin, self).dispatch(*args, **kwargs)


class PostSourceView(CSRFExemptMixin, View):
    def post(self, request, *args, **kw):
        # Import the data
        importer = Importer()
        importer.import_request(request)
        return importer.results.to_response()


class TestPostSourceView(CSRFExemptMixin, View):
    def post(self, request, *args, **kw):
        # Import the data
        importer = Importer()
        importer.test_request(request)
        return importer.results.to_response()


class ExportSources(VisitorMixin, View):
    def get(self, request, *args, **kw):
        if not self.visitor or not self.visitor.is_superuser:
            data = cmodels.Source.export_json()
        else:
            data = cmodels.Source.export_json(with_tokens=True)

        res = http.HttpResponse(content_type="application/json")
        json.dump(data, res, indent=2)
        return res
