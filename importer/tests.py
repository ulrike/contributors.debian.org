from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from contributors import models as bmodels
from dc.unittest import SourceFixtureMixin
from unittest import mock
from . import importer
from io import BytesIO
import datetime
import json


class ImportTest(SourceFixtureMixin, TestCase):
    def make_submission(self, type="email", name="enrico@enricozini.org", begin=None, until=None, ident=None):
        if ident is not None:
            type = ident.type
            name = ident.name
        contr = {
            "type": "tester",
            "url": "http://example.org/tester/" + name,
        }
        if begin:
            contr["begin"] = begin
        if until:
            contr["end"] = until
        res = {
            "id": [{
                "type": type,
                "id": name,
            }],
            "contributions": [contr],
        }
        return res

    def make_request(self, data, **extra_post_args):
        factory = RequestFactory()
        submission = BytesIO(json.dumps(data).encode("utf-8"))
        submission.name = "submission.json"
        return factory.post(reverse("contributors_post"), data={
            "source": self.sources.test.name,
            "auth_token": self.sources.test.auth_token,
            "data": submission,
            **extra_post_args,
        })

    def assertImporterSuccess(self, importer, created=0, updated=0, unchanged=0):
        self.assertEqual(importer.results.code, 200)
        self.assertEqual(importer.results.identifiers_skipped, 0)
        self.assertEqual(importer.results.contributions_processed, created + updated + unchanged)
        self.assertEqual(importer.results.contributions_created, created)
        self.assertEqual(importer.results.contributions_updated, updated)
        self.assertEqual(importer.results.contributions_skipped, 0)
        self.assertEqual(importer.results.records_parsed, created + updated + unchanged)
        self.assertEqual(importer.results.records_skipped, 0)
        self.assertEqual(importer.results.errors, [])

    def _test_import_create(self, method=None):
        kwargs = {}
        if method is not None:
            kwargs["method"] = method
        req = self.make_request([self.make_submission(begin="2013-08-11", until="2013-08-18")], **kwargs)

        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertImporterSuccess(i, created=1)

        # See that the identifier was created
        identifier = bmodels.Identifier.objects.get(type="email", name="enrico@enricozini.org")
        self.assertEqual(identifier.hidden, False)

        # See that the contribution was created
        contrib = bmodels.Contribution.objects.get(identifier=identifier, type=self.ctypes.tester)
        self.assertEqual(contrib.begin, datetime.date(2013, 8, 11))
        self.assertEqual(contrib.until, datetime.date(2013, 8, 18))
        self.assertEqual(contrib.url, "http://example.org/tester/enrico@enricozini.org")

    def test_import_create_default(self):
        self._test_import_create()

    def test_import_create_replace(self):
        self._test_import_create(method="replace")

    def test_import_create_extend(self):
        self._test_import_create(method="extend")

    def _do_import_update(self, ident="dd_user", begin=None, until=None, method=None, updated=1, unchanged=0):
        ident = self.idents[ident]
        submission = self.make_submission(ident=ident, begin=begin, until=until)
        if method is not None:
            req = self.make_request([submission], method=method)
        else:
            req = self.make_request([submission])
        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertImporterSuccess(i, updated=updated, unchanged=unchanged)

    def assertContribRange(self, ident, begin, until):
        contrib = bmodels.Contribution.objects.get(identifier=ident, type=self.ctypes.tester)
        self.assertEqual(contrib.begin, begin)
        self.assertEqual(contrib.until, until)
        self.assertEqual(contrib.url, "http://example.org/tester/" + ident.name)

    def test_import_update_default(self):
        # If method= is missing, replace is used
        self._do_import_update(begin="2014-02-02", until="2014-02-14")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  2), datetime.date(2014, 2, 14))

    def test_import_update_extend(self):
        self._do_import_update(begin="2014-02-02", until="2014-02-14", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 2, 15))

        self._do_import_update(begin="2014-01-15", until="2014-02-14", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 1, 15), datetime.date(2014, 2, 15))

        self._do_import_update(begin="2014-01-16", until="2014-02-20", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 1, 15), datetime.date(2014, 2, 20))

        self._do_import_update(begin="2014-01-10", until="2014-03-15", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 1, 10), datetime.date(2014, 3, 15))

    def test_import_update_extend_begin(self):
        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 15)
            self._do_import_update(begin="2014-02-02", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 3, 15))

        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 10)
            self._do_import_update(begin="2014-01-15", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 1, 15), datetime.date(2014, 3, 15))

    def test_import_update_extend_until(self):
        self._do_import_update(until="2014-02-14", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 2, 15))

        self._do_import_update(until="2014-03-01", method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 3,  1))

    def test_import_update_extend_instant(self):
        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 15)
            self._do_import_update(method="extend")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 3, 15))

        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 10)
            self._do_import_update(method="extend", updated=0, unchanged=1)
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 3, 15))

    def test_import_update_replace(self):
        self._do_import_update(begin="2014-02-02", until="2014-02-14", method="replace")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  2), datetime.date(2014, 2, 14))

    def test_import_update_replace_begin(self):
        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 15)
            self._do_import_update(begin="2014-02-02", method="replace")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  2), datetime.date(2014, 3, 15))

    def test_import_update_replace_until(self):
        self._do_import_update(until="2014-02-14", method="replace")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 2, 14))

    def test_import_update_replace_instant(self):
        with mock.patch('contributors.models._today') as today:
            today.return_value = datetime.date(2014, 3, 15)
            self._do_import_update(method="replace")
        self.assertContribRange(self.idents.dd_user, datetime.date(2014, 2,  1), datetime.date(2014, 3, 15))

    def test_empty_import(self):
        req = self.make_request([])
        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertImporterSuccess(i, created=0, updated=0)

    def test_failed_imports(self):
        """
        Test a simple import run
        """
        submission = self.make_submission(begin="2013-08-11", until="2013-08-18")
        req = self.make_request([submission], source="fail")
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEqual(i.results.code, 404)

        req = self.make_request([submission], auth_token="fail")
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEqual(i.results.code, 403)


class SourceImportTest(TestCase):
    def test_empty_import(self):
        bmodels.Source.import_json([])

    def test_simple_import(self):
        source = bmodels.Source(name="www", desc="www.debian.org", url="http://www.debian.org", auth_token="12345")
        source.save()
        contrib = bmodels.ContributionType(
            source=source,
            name="commit",
            desc="www.debian.org webml commits",
            contrib_desc="Website committer")
        contrib.save()

        sources = bmodels.Source.export_json(with_tokens=True)
        source.delete()
        bmodels.Source.import_json(sources)

        source = bmodels.Source.objects.get(name="www")
        self.assertEqual(source.name, "www")
        self.assertEqual(source.desc, "www.debian.org")
        self.assertEqual(source.url, "http://www.debian.org")
        self.assertEqual(source.auth_token, "12345")
        contrib = source.contribution_types.all()[0]
        self.assertEqual(contrib.name, "commit")
        self.assertEqual(contrib.desc, "www.debian.org webml commits")
        self.assertEqual(contrib.contrib_desc, "Website committer")
