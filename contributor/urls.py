from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^claim_email$', views.ClaimEmail.as_view(), name='contributor_claim_email'),
    url(r'^verify_claim$', views.VerifyClaim.as_view(), name='verify_claim'),
    url(r'^(?P<name>[^/]+)/$', views.ContributorView.as_view(), name='contributor_detail'),
    url(r'^(?P<name>[^/]+)/save_settings$', views.SaveSettings.as_view(), name="contributor_save_settings"),
    url(r'^(?P<name>[^/]+)/impersonate$', views.Impersonate.as_view(), name="contributor_impersonate"),
    url(r'^(?P<name>[^/]+)/unclaim$', views.Unclaim.as_view(), name="contributor_unclaim"),
]
