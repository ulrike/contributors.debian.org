Debian Contributors web application
===================================

See https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel for this project's page in the Debian wiki.

## Running this code on your own machine

### Dependencies

    apt-get install python3-ldap python3-debiancontributors python3-django-housekeeping python3-django python3-djangorestframework eatmydata
      (eatmydata is not necessary but it will make everything run much faster 
       in a test environment)

### Configuration

    cp dc/local_settings.py.devel dc/local_settings.py
    edit dc/local_settings.py as needed

### First setup

    ./manage.py migrate

You may wish to add an admin user that will be named the same as the `TEST_USER` (see Authentication below).

### Fill in data

Import source information from http://contributors.debian.org/contributors/export/sources
if you are not logged as a DD, authentication tokens are replaced with dummy
ones:

    curl https://contributors.debian.org/contributors/export/sources | eatmydata ./manage.py import_sources

    eatmydata ./manage.py runserver

From now on, imported sources will be available from http://localhost:8000/sources/. 

You may login to http://localhost:8000/admin/ which grants access to the sources and other tables. (currently broken)

Go to https://contributors.debian.org/sources/, choose a data source, click on
"Members", then "Add me as member": you'll then be able to access the data
source configuration. From there you can see the data source name and the auth
token for posting.

Acquire some JSON data (ask Enrico at the moment) and import it:

    dc-tool --post --baseurl=http://localhost:8000 --source sourcename --auth-token sourcetoken datafile.json

Sync Debian keyrings into data/keyrings/ :

    ./synckeyrings.sh

    eatmydata ./manage.py maintcarnivore

### Run housekeeping

Update the alioth and debian UIDs, and create Django user accounts accordingly:

    # ssh alioth.debian.org -L 3389:localhost:389
    eatmydata ./manage.py housekeeping

### Run the web server

    eatmydata ./manage.py runserver

## Development

Development targets the current Debian Stable plus backports, and that
determines the version of Django and the dependencies that can be used.

You can find a TODO list at https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel

## Authentication

Authentication in production is meant to be performed by Apache, so the
application relies on RemoteUserMiddleware, taking the user name from the
`SSL_CLIENT_S_DN_CN` env var so that it comes from Debian Single Signon certificates.

For testing purposes, you can set `settings.TEST_USER` to override
`SSL_CLIENT_S_DN_CN`.

